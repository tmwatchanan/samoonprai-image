from PIL import Image
import os
import pathlib
import random

TO_EXTENSION = 'jpg'  # png

secure_random = random.SystemRandom()

image_directory_path = os.path.join(os.getcwd(), 'herb_images')
image_directory_list = [name for name in os.listdir(image_directory_path) if
                        os.path.isdir(os.path.join(image_directory_path, name))]
print(image_directory_list)
for img_dir in image_directory_list:
    img_dir_path = os.path.join(image_directory_path, img_dir)
    image_file_list = [name for name in os.listdir(img_dir_path) if os.path.isfile(os.path.join(img_dir_path, name))]
    while True:
        img_file  = secure_random.choice(image_file_list)
        file_list = [name for name in os.listdir(img_dir_path) if os.path.isfile(os.path.join(img_dir_path, name))]
        count_image = len(file_list)
        if count_image >= 20:
            break
        img_file_path = os.path.join(img_dir_path, img_file)
        # print(img_file_path)

        print(img_file_path)
        im = Image.open(img_file_path)
        if TO_EXTENSION == 'jpg':
            file_name = str(count_image + 1) + '.' + TO_EXTENSION
            new_im_file_path = os.path.join(img_dir_path, file_name)
            rgb_im = im.convert('RGB')
            rgb_im.save(new_im_file_path)
        elif TO_EXTENSION == 'png':
            img_file_name = os.path.splitext(img_file)[0] + '.' + TO_EXTENSION
            new_im_file_path = os.path.join(img_dir_path, img_file_name)
            im.save(new_im_file_path)
